
import pygame
from . import objectmanager

class Window:
    WIDTH = 800
    HEIGHT = 600
    TITLE = "Задача на работу с кодом (ООП)"

    def __init__(self):
        pygame.init()
        self.surface = pygame.display.set_mode((Window.WIDTH, Window.HEIGHT))
        pygame.display.set_caption(Window.TITLE)
        self.running = False
        self.clock = pygame.time.Clock()

        self.object_manager = objectmanager.ObjectManager()
    
    def update(self):
        self.object_manager.update()

    def draw(self):
        self.object_manager.draw(self.surface)
    
    def handle_click(self, mouse_x, mouse_y):
        self.object_manager.handle_click(mouse_x, mouse_y)
    
    def run(self):
        self.running = True
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.handle_click(*pygame.mouse.get_pos())
            
            self.update()

            self.surface.fill((255, 255, 255))
            self.draw()
            pygame.display.flip()
            self.clock.tick(60)
