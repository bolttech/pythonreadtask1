
import random
from . import window
from .circleobject import CircleObject
from .squareobject import SquareObject

class ObjectManager:
    MAX_SPAWN_COUNTER = 60

    def __init__(self):
        self.objects = []
        self.spawn_counter = ObjectManager.MAX_SPAWN_COUNTER
    
    def spawn_object(self):
        if random.randint(0, 1) == 1:
            self.objects.append(self.spawn_circle())
        else:
            self.objects.append(self.spawn_square())

    def spawn_circle(self):
        radius = random.randint(5, 25)

        if random.randint(0, 1) == 1:
            center_x = random.randint(-70, -50)
        else:
            center_x = random.randint(50, 70) + window.Window.WIDTH
        
        if random.randint(0, 1) == 1:
            center_y = random.randint(-70, -50)
        else:
            center_y = random.randint(50, 70) + window.Window.HEIGHT
        
        return CircleObject(center_x, center_y, radius)
    
    def spawn_square(self):
        side_width = random.randint(5, 25)

        if random.randint(0, 1) == 1:
            top_left_x = random.randint(-70, -50)
        else:
            top_left_x = random.randint(50, 70) + window.Window.WIDTH
        
        if random.randint(0, 1) == 1:
            top_left_y = random.randint(-70, -50)
        else:
            top_left_y = random.randint(50, 70) + window.Window.HEIGHT
        
        return SquareObject(top_left_x, top_left_y, side_width)
    
    def spawn_if_needed(self):
        self.spawn_counter -= 1
        if self.spawn_counter <= 0:
            self.spawn_object()
            self.spawn_counter = ObjectManager.MAX_SPAWN_COUNTER
    
    def remove_if_needed(self):
        self.objects[:] = [ obj for obj in self.objects if not obj.is_outside() ]
    
    def update(self):
        self.spawn_if_needed()

        for obj in self.objects:
            obj.update()
        
        self.remove_if_needed()
    
    def draw(self, surface):
        for obj in self.objects:
            obj.draw(surface)
    
    def handle_click(self, mouse_x, mouse_y):
        self.objects[:] = [obj for obj in self.objects if not obj.is_hit(mouse_x, mouse_y)]
