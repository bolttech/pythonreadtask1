
import random
import pygame
from . import window
from .gameobject import GameObject

class SquareObject(GameObject):
    
    def __init__(self, top_left_x, top_left_y, side_width):
        self.top_left_x = top_left_x
        self.top_left_y = top_left_y
        self.side_width = side_width

        if top_left_x + side_width // 2 < window.Window.WIDTH // 2:
            self.delta_x = random.randint(1, 5)
        else:
            self.delta_x = random.randint(-5, -1)
        
        if top_left_y - side_width // 2 < window.Window.HEIGHT // 2:
            self.delta_y = random.randint(1, 5)
        else:
            self.delta_y = random.randint(-5, -1)
    
    def update(self):
        self.top_left_x += self.delta_x
        self.top_left_y += self.delta_y
    
    def draw(self, surface):
        pygame.draw.rect(
            surface,             # на чем
            (0, 0, 0),           # какого цвета
                                 # прямоугольник: (x, y, w, h)
            (self.top_left_x, self.top_left_y, self.side_width, self.side_width),
            1                    # толщина линии
        )
    
    def is_hit(self, x, y):
        return x >= self.top_left_x and                  \
            x <= self.top_left_x + self.side_width and   \
            y >= self.top_left_y and                     \
            y <= self.top_left_y + self.side_width
    
    def is_outside(self):
        return self.top_left_x > window.Window.WIDTH + 100 or   \
            self.top_left_x < -100 or                    \
            self.top_left_y > window.Window.HEIGHT + 100 or     \
            self.top_left_y < -100
