
class GameObject:

    def update(self):
        raise NotImplementedError()
    
    def draw(self, surface):
        raise NotImplementedError()
    
    def is_hit(self, x, y):
        raise NotImplementedError()
    
    def is_outside(self):
        raise NotImplementedError()
