
import math
import random
import pygame
from . import window
from .gameobject import GameObject

class CircleObject(GameObject):

    def __init__(self, center_x, center_y, radius):
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius

        if center_x < window.Window.WIDTH // 2:
            self.delta_x = random.randint(1, 5)
        else:
            self.delta_x = random.randint(-5, -1)
        
        if center_y < window.Window.HEIGHT // 2:
            self.delta_y = random.randint(1, 5)
        else:
            self.delta_y = random.randint(-5, -1)

    def update(self):
        self.center_x += self.delta_x
        self.center_y += self.delta_y
    
    def draw(self, surface):
        pygame.draw.circle(
            surface,                          # на чем
            (0, 0, 0),                        # какого цвета
            (self.center_x, self.center_y),   # где
            self.radius,                      # какого радиуса
            1                                 # толщина линии
        )
    
    def is_hit(self, x, y):
        return math.sqrt((self.center_x - x)**2 + (self.center_y - y)**2) <= self.radius
    
    def is_outside(self):
        return self.center_x > window.Window.WIDTH + 100 or   \
            self.center_x < -100 or                           \
            self.center_y > window.Window.HEIGHT + 100 or     \
            self.center_y < -100
